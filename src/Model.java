import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Model {
    private int number;
    private final int RAND_MAX = 100;
    private int max = RAND_MAX;
    private int min = 0;

    Model() {
    }



    public void setNumber() {
        this.number = rand(0, RAND_MAX);
    }

    public void setNumber(int min, int max) {
        this.number = rand(min, max);
    }

    public void setNumber(String min, String max) {
        Pattern p = Pattern.compile("^[1-9]+\\d*$");
        Matcher matcherMin = p.matcher(min);
        if (matcherMin.find()) {
            Matcher matcherMax = p.matcher(max);
            if (matcherMax.find()) {
                this.min = Integer.parseInt(min);
                this.max = Integer.parseInt(max);
                this.number = rand(this.min, this.max);
            }
        }
    }

    public int getNumber() {
        return this.number;
    }

    public int getMax(){
        return this.max;
    }

    public int getMin(){
        return  this.min;
    }

    private int rand(int min, int max) {
        Random rand = new Random();

        System.out.println("min = " + this.min);
        return rand.nextInt(min, max);
    }
}
