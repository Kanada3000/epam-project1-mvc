import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {
    private Model model;
    private View view;

    Controller() {
    }

    Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void run() {
        view.greting();
        view.instruction();
        setRange();
        System.out.println("NUMBER IS " + model.getNumber());
        view.start(model.getMin(), model.getMax());
        guessing();

    }

    public void setRange() {
        Scanner sc = new Scanner(System.in);
        String range = sc.nextLine();
        if (!range.isEmpty()) {
            model.setNumber(range.split(" ")[0], range.split(" ")[1]);
        }
    }

    public void guessing() {
        Scanner sc = new Scanner(System.in);
        boolean isGuess = false;
        String guessString = "";
        int guessNumber;
        int wrongNumber = 0;
        int tryings = 0;
        Pattern p = Pattern.compile("^[1-9]+\\d*$");
        Matcher m;
        while (!isGuess) {
            tryings++;
            while (sc.hasNextLine()) {
                guessString = sc.nextLine();
                m = p.matcher(guessString);
                if (!m.find()) {
                    wrongNumber++;
                    view.error(model.getMin(), model.getMax());
                    continue;
                }
                break;
            }
            guessNumber = Integer.parseInt(guessString);
            if(guessNumber < model.getMin() || guessNumber > model.getMax()){
                wrongNumber++;
                view.error(model.getMin(), model.getMax());
                continue;
            }
            if (guessNumber > model.getNumber()) view.less();
            else if (guessNumber < model.getNumber()) view.grater();
            else {
                view.win(tryings, wrongNumber);
                isGuess = true;
            }

        }
    }
}
