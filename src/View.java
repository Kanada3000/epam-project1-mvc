public class View {
    private final String GREETING = "Hi, can we play?";
    private final String START = "Okay, I guess a number in range [%d, %d].%nLet`s play! Enter a number...%n";
    private final String INSTRUCTION = "Please, type the range (min max) or press Enter for [0; 256]";
    private final String GRATER = "My number is grater than yours";
    private final String LESS = "My number is less than yours";
    private final String WIN = "YOU WIN!";
    private final String ERROR = "Sorry, but you can type only numbers from range [%d; %d]%n";
    private final String STATISTIC = "You are trying to guess %d times. You typed wrong data %d times%n";

    public void greting(){
        System.out.println(GREETING);
    }

    public void grater(){
        System.out.println(GRATER);
    }

    public void start(int min, int max){
        System.out.printf(START, min, max);
    }

    public void instruction(){
        System.out.println(INSTRUCTION);
    }

    public void less(){
        System.out.println(LESS);
    }

    public void error(int min, int max){
        System.out.printf(ERROR, min, max);
    }

    public void win(int trying, int wrong){
        System.out.println(WIN);
        System.out.printf(STATISTIC, trying, wrong);
    }
}
